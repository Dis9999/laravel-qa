<?php

use Faker\Generator as Faker;

$factory->define(LQA\Question::class, function (Faker $faker) {
    return [
        'title' => rtrim($faker->sentence(rand (5,10)), "."),
        // paragraphs converted to string, if true as a second argument set
        'body' => $faker->paragraphs(rand(3,7), true),
        'views' => rand(0,10),
        'answers' => rand(0,10),
        'votes' => rand(-3,10)
    ];
});
