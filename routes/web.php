<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

// Route::get('/', 'HomeController@index')->name('home');
Route::group(['middleware' => ['web']], function () {
    Route::get('/', [
        'as' => 'home',
        'uses' => 'HomeController@index'
    ]);

    Route::get('/form', function () {
        return view('form');
    });

    Route::get('/index','UserDetailController@index');
    Route::post('submitForm','UserDetailController@store');
    // download pdf
    Route::get('/downloadPDF/{id}','UserDetailController@downloadPDF');
});

Auth::routes();




